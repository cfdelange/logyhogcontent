# Logyhog on Kubernetes Windows 10

Logyhog Kubernetes deployment is still evolving. As it stands now. Logyhog for Windows Docker desktop Kubernetes consists of:

1. 4 Deployments
2. 4 Headless services
3. 1 Ingress
4. 2 Configmaps
5. 1 Optional test application to test the api pods and this documentation

## Run Logyhog with docker-desktop kubernetes

### Apply the mandatory ingress commands

> If you were wondering why you need the mandatory commands. It is part of the standard Kubernetes ingress install as found
> in Kubernetes ingress install documentation.

#### Services, configmaps and ingress controller (Mandatory)

```powershell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.28.0/deploy/static/mandatory.yaml
```

***TIP: Custom nginx.conf (Not mandatory)*** The above mandatory command creates a config map that could be of interest when adding custom variable to the ingress.
For this version it is called: **configmap/nginx-configuration** the name gets printed to screen when the yaml is applied.
***Example:*** To use that config map name handle and add a http snippet to the ingress controller /etc/nginx/nginx.conf, you can give your configmap the same name and deploy it:

```yml
kind: ConfigMap
apiVersion: v1
metadata:
  name: nginx-configuration
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
data:
  http-snippet: |
    include /etc/nginx/mime.types;
    types {
       application/wasm wasm;
    }
```

#### Set Nginx Loadbalancer service (Mandatory)

```powershell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.28.0/deploy/static/provider/cloud-generic.yaml
```

##### From here on the setup is specific to Logyhog

### Setup the configmaps

The logmini config map has the password to the MySQL server. The server port is not exposed to the outside. In production it password should be created in a Kubernetes secret.

```powershell
kubectl create configmap logmini --from-file=logmini_appsettings
```

This turns Google Recaptcha off for the windows install.

```powershell
kubectl create configmap logmini --from-file=logstuff_appsettings
```

### Deploy the rest of the application with kubectl

Navigate to the folder where the deployment .yaml files are stored (To deploy all in the folder)
kubectl apply -f .

### On Windows edit file hosts for Windows DNS to work with the ingress controller

C:\windows\system32\drivers\etc.  
In file: ***hosts***  
add: ***127.0.0.1 loghog.com api.loghog.com***

## Kubernetes dashboard on Docker desktop

Viewing the deployed application on kubernetes dashboard. Run all powershell instances as Admin for the below commands.

### Starting the dashboard (Never versions are available)

* Deploy the dashboard

    ```powershell
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
    ```

* Open a new PowerShell window and run:

    ```powershell
    kubectl proxy
    ```

    leave this powershell window open. Use another powershell window for further commands.  

* **In a second PowerShell window:**

    ```powershell
    $TOKEN=((kubectl -n kube-system describe secret default | Select-String "token:") -split " +")[1]
    ```

* Set the credentials for docker-desktop user

    ```powershell
    kubectl config set-credentials docker-desktop --token="${TOKEN}"  
    ```

* **Go to your browser and enter the below url:**

    ```powershell
    localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
    ```

To sign into the dashboard: Choose the Kube config option, click on the "Choose kube config file"
 font in the sign in screen and navigate to your to the .kube
folder most likely in **C:\Users\<your user name>\.kube** click on the **config** file for dashboard to load it.  
You should have access now.
