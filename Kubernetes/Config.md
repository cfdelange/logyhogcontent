# Kubernetes config and kubectl

With DigitalOcean.com you need doctl (digital ocean ctl) to manage config files and set the context.
DigitalOcean has good tutorials for all you need to know. Below are some quick commands that I use in cluster management.

## doctl

After installation of **doctl** I added it to my path. To use **doctl** you need to create a API token with the DigitalOcean dashboard. You only need a read-only DigitalOcean API token to set everything up. Kubernetes cluster object creation is done through kubectl in the cluster and authentication to do this resides in the **.kube** folder under your Windows user name. In a file called **config**.

### To check doctl authentication

I always run powershell as administrator when do these commands.

```powershell
doctl auth init
```

From the above command you should get a message saying what token is being used and **Validating token... OK**

### Merge kubectl config file use

```powershell
doctl kubernetes cluster kubeconfig save <cluster name>
```

Test if you can check the cluster by running something like:

```powershell
kubectl get pods
```

If you have any pods running it should be displayed on screen.

### If DigitalOcean API token change

I deleted my DigitalOcean API token once in the DigitalOcean dashboard. I created a new read only token and had to put the token in my doctl file and remove the old token that I deleted. 
On my PC the token went into this folder **C:\Users\<user>\AppData\Roaming\doctl** in a file called **config.yaml**.

## Kubectl config commands

Check the **current active context** run:

```powershell
kubectl config current-context
```

Get all the available context. On Window 10 if you run Kubernetes with docker-for-desktop you will see that context and any other context that you have added.

```powershell
kubectl config get-contexts
```

The active context is specified with an asterisk under “CURRENT”.
When you use kubectl, the commands you run affect the current context unless you specify a different one with the --context flag (for example, kubectl get nodes --context=sfo-nyc100-dev)
To **set the current context to a different one**, use:
For example the docker-desktop context:

```powershell
kubectl.exe config use-context docker-desktop
```

## Unset a cluster from kube config

If you deleted a cluster and want to remove it from the kube config.

Delete cluster from kube config.

```powershell
kubectl config delete-cluster my-cluster
```

Delete the context from kube config.

```powershell
kubectl config delete-context my-cluster-context
```

There's nothing specific for users, use

```powershell
kubectl config unset users.my-cluster-admin
```
