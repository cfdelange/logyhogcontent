# Ingress on DigitalOcean

The following steps describe some steps to deploy ingress on DigitalOcean. There are good tutorials out there, the notes below is just a quick reference.

## Favorite Debug commands first

Before any deployment I like my favorite debug commands on top of the list. This command lets you view the nginx.conf for the ingress controller.

```powershell
kubectl exec nginx-ingress-controller-full-name -n ingress-nginx cat /etc/nginx/nginx.conf
```

The -n spicifies the namespace where the controller will run

## DigitalOcean ingress start

The steps are pretty much the same as for Windows 10 docker-desktop ingress up to a point. You still need the two mandatory deployment steps.
**Some of these command will deploy older version of what is currently available.** But these version were available at the time of writing this.

### Step 1

```powershell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.28.0/deploy/static/mandatory.yaml
```

Command **Output**:

```powershell
namespace/ingress-nginx created **Note: Namespace created where ingress controller will reside**
configmap/nginx-configuration created **Note: use this config map name if you want add custom ingress annotations/http snippet**
configmap/tcp-services created
configmap/udp-services created
serviceaccount/nginx-ingress-serviceaccount created
clusterrole.rbac.authorization.k8s.io/nginx-ingress-clusterrole created
role.rbac.authorization.k8s.io/nginx-ingress-role created
rolebinding.rbac.authorization.k8s.io/nginx-ingress-role-nisa-binding created
clusterrolebinding.rbac.authorization.k8s.io/nginx-ingress-clusterrole-nisa-binding created
deployment.apps/nginx-ingress-controller created
```

### Step 2

This sets up the Nginx Ingress Loadbalancer service.  (DNS records points to this service external IP address)

```powershell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.28.0/deploy/static/provider/cloud-generic.yaml
```

Command **Output**:

service/ingress-nginx created

Quick **test**:

* Confirm ingress controller pods

```powershell
kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx
```

* Confirm that the DigitalOcean load balancer was created and ip:

```powershell
kubectl get svc --namespace=ingress-nginx
```

Command **Output**:
The output shows NAME, TYPE, CLUSTER-IP, EXTERNAL-IP etc. Point DNS A-records to the EXTERNAL-IP shown.

> Berfore starting on Step 3 I created 3 folders on my pc. Staging1,
> Staging2 and Production. All containing the same file names.

```powershell
ingress project
│   README.md
│
└───Staging1
│   │   ingress.yaml
│   │   issuer.yaml
│   |   namespace-cert-manager.yaml
|
└───Staging2
│   │   ingress.yaml
│   │   issuer.yaml
│   |   namespace-cert-manager.yaml
│
└───Production
    │   ingress.yaml
    │   issuer.yaml
    |   namespace-cert-manager.yaml
```

### Step 3

Creating the custom ingress resource from yaml and installing/Configuring Cert-Manager.

### Staging 1

**Staging 1**: Custom in ingress here in **ingress.yaml**

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: custom-ingress-name
  namespace: default
  annotations:
      nginx.ingress.kubernetes.io/ingress.class: nginx
spec:
  rules:
  #Your website name here
  - host: www.website-name.com
    http:
      paths:
      - backend:
          #Your app pod name here to be discovered by kube dns
          serviceName: podname-for-k8sDNS
          servicePort: 80
```

1. Navigate to the folder Staging1. Run below command:

    ```powershell
    kubectl apply -f ingress.yaml
    ```

2. Create the namespace for cert-manager
    **Staging 1**: **namespace-cert-manager.yaml**

    ```yaml
    apiVersion: v1
    kind: Namespace
    metadata:
        name: cert-manager
        labels:
          environment: development
        annotations:
          certmanager.k8s.io/disable-validation : "true"
    ```

    ```powershell
    kubectl apply -f namespace-cert-manager.yaml
    ```

3. Install cert-manager and its Custom Resource Definitions (CRDs)
    Apply the manifest from the github repository. Newer versions will be available.

    ```powershell
    kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.0/cert-manager.yaml
    ```

    **TEST:** Check the namespace after creation:

    ```powershell
    kubectl get pods --namespace cert-manager
    ```

    **Result:**
    In my case 3 cert manager application pods were created.

4. Create the test issuer to test provisioning mechanism.

    **Staging 1**: **issuer.yaml**

    ```yaml
    apiVersion: cert-manager.io/v1alpha2
    kind: ClusterIssuer
    metadata:
     name: letsencrypt-staging
     namespace: cert-manager
    spec:
     acme:
       # The ACME server URL
       server: https://acme-staging-v02.api.letsencrypt.org/directory
       # Email address used for ACME registration
       email: Your-email.com
       # Name of a secret used to store the ACME account private key
       privateKeySecretRef:
         name: letsencrypt-staging
       # Enable the HTTP-01 challenge provider
       solvers:
       - http01:
           ingress:
             class:  nginx
    ```

    ```powershell
    kubectl create -f issuer.yaml
    ```

### Staging 2

**Staging 2**: Custom in ingress here in **ingress.yaml**

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: custom-ingress-name
  namespace: default
  annotations:
      nginx.ingress.kubernetes.io/ingress.class: nginx
      cert-manager.io/cluster-issuer: "letsencrypt-staging"
spec:
  tls:
  - hosts:
    #Your website name
    - www.website-name.com
    secretName: echo-tls
  rules:
  - host: www.website-name.com
    http:
      paths:
      - backend:
          serviceName: podname-for-k8sDNS
          servicePort: 80
```

**Staging 2**: **issuer.yaml**

```yaml
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
 name: letsencrypt-staging
 namespace: cert-manager
spec:
 acme:
   # The ACME server URL
   server: https://acme-staging-v02.api.letsencrypt.org/directory
   # Email address used for ACME registration
   email: Your-email.com
   # Name of a secret used to store the ACME account private key
   privateKeySecretRef:
     name: letsencrypt-staging
   # Enable the HTTP-01 challenge provider
   solvers:
   - http01:
       ingress:
         class:  nginx
```

**Staging 2**: **namespace-cert-manager.yaml**

```yaml
apiVersion: v1
kind: Namespace
metadata:
    name: cert-manager
    labels:
      environment: development
    annotations:
      certmanager.k8s.io/disable-validation : "true"
```

### Production

**Production**: **ingress.yaml**

```yaml
#Run: kubectl create -f ingress.yaml
#
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: custom-ingress-name
  namespace: default
  annotations:
      nginx.ingress.kubernetes.io/ingress.class: nginx
      #Note the prod in "letsencrypt-prod"
      cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - www.website-name.com
    secretName: echo-tls
  rules:
  - host: www.website-name.com
    http:
      paths:
      - backend:
          serviceName: podname-for-k8sDNS
          servicePort: 80
```

**Production**: **issuer.yaml**

```yaml
# kubectl create -f prod_issuer.yaml
# Create a production certificate ClusterIssuer.
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
  namespace: cert-manager
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: Your-email.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-prod
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx
```

**Production**: **namespace-cert-manager.yaml**

```yaml
apiVersion: v1
kind: Namespace
metadata:
    name: cert-manager
    labels:
      environment: development
    annotations:
      certmanager.k8s.io/disable-validation : "true"
```
