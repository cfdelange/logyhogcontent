# About the Logyhog project

Logyhog is an open source dashboard and logging application. The application accepts basic telemetry logged to its API and serve the data up in a dashboard format. The telemetry formats are Y data, (X, Y) data, Text data and Rectangle coordinates (X, Y, Width, Height). The Logyhog project is made up by a group of micro services that work together. The various micro services are discussed below.
Any user can host the Logyhog project on Kubernetes and details to do so are included in the documentation.

## Where would LogyHog fit into a system

![image info](/images/lotsofdevicesanddbs.jpg)

The above image shows that LogyHog is not designed to receive high speed bulk data. LogyHog is there to receive processing outcomes on data. Where data is turned into information.

## Error checking and warnings with LogyHog

You can set data PASS/FAIL limits in the LogyHog API by logging Rectangle coordinates for a dataset. If the XY data does not fall into that Rectangle then it will set the DataSet to Fail on the dashboard.

The Below figure shows the rectangle and the failed XY data point.

![image info](/images/XYFail.png)

The Datasets that fail will show up like this in the dashboard. See below. Each button represents a dataset. Where you can drill down into the dataset.

![image info](/images/dash.png)

## Logyhog Project overview

Logyhog is a **ASP .NET Core** project consisting of these micro services:

* The head of the project is a MVC ASP ,NET Core application.
* Two API's provide access to the database. One API is used be the MVC application and is not exposed to the outside word. The other API is for user interaction.
* Logyhog docs (this documentation) is a **Blazor** application.

* Drivers: There are also two drivers to interact with the user API. One is in Python and the other is in the .NET standard C#.

![image info](/images/appsummary.jpg)

Project Deployment:

* **Kubernetes** is used to deploy and run the application, the project consists of five containers, that can be launched with .yml files. *For development:* Logyhog can run on **Kubernetes\Docker Desktop for Windows 10** see Deploy on Windows10 documentation.
* The data is stored in a MySQL database. In its own Pod.
* API drivers are used to populate data effectively and leverage the API functionality, there are two API drivers one in **Python 3** and one in **.netstandard 2.1**. See more on the logging assist libraries under  **Logging Libraries** in this documentation.
* The documentation you are reading now is served up with a **Blazor** application.

### Ingress

Ingress has its own section in docs called **Ingress**
