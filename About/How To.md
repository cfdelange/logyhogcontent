# LogyHog project

## How to page

General questions and answer that are referenced to other documentation.

### How to run the LogyHog project?

Run on Kubernetes. Development and testing can be done on the Windows 10 Kubernetes environment. See Deploy - Windows10 in this documentation.  

### How to create a project or task?

Projects and tasks get created in the LogyHog browser view. The pod that provides the browser view is logstuff. See About - LogyHog - LogyHog Project overview diagram, in this documentation.
How many projects and tasks depends on how you want to organize your work.
See Logging - Python3 for some screen shots on **new Project**.

- A user can have multiple projects.
- A project can contain multiple tasks.

### How to create a Dataset?

Datasets gets created by using the API. You cannot create a dataset through a web browser View. See **Logging - Python3 - How to create and log to LogyHog datasets**.

### How to log data?

See **Logging - Python3 - How to create and log to LogyHog datasets**. Creating a dataset and logging data is the same command, using the api driver look in the example code in Logging - Python3 where it says: logyhogLogger.y("myDataset", yValue) at this point if "myDataset" does not exist then the api driver will create it for you.

### How does authentication work when logging to the API?

I random token gets generated when the dataset is created by the API. This token is then used by the api driver to get access. The token gets resolved in then API middleware to give access to a command to log the data point.
