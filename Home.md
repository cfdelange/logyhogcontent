# Documents for the LogyHog project

This documents application contains a mix of engineering notes and user notes for the LogyHog project. A lot of the notes also serves as reference to make continuous development and work on the project easier.

***
***

## Some notes on this document application itself

docs.Logyhog (the documents you are reading now) is a Blazor application. The docs application container runs in a pod in Kubernetes in the LogyHog project cluster.
The documents are written in Markdown and the converted to HTML when served up for the user/reader.

## Markdown to HTML conversion code

```csharp
    public async Task LoadBlogPost(string filename)
    {
        string textfromfile = await System.IO.File.ReadAllTextAsync(@$"./Content/{filename}.md");
        // Configure the pipeline with all advanced extensions active
        var pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().Build();
        html = (MarkupString)Markdown.ToHtml(textfromfile, pipeline);
    }
```

## Automate docs delivery

![image info](/images/gittodocs.jpg)

The **Markdown** files are stored in a git repository. So when a new markdown file is loaded to the **git repository** it is ready to be pulled into the pod volume where it can be displayed as **HTML**. The files in the pod volume will be updated when the pod is restarted. (Delete the pod and the Deployment backed by the ReplicaSet creates a new one.) I have not yet implemented the helper container that will check for updates in the Git repository.

## docs Logyhog tests

docs Logyhog has another purpose, it shows the status of the Logyhog deployment. When you explore the App Status area you should see all pass values else there is something wrong with the deployment.
The docs Logyhog uses the HttpsNet21Log driver available on nuget to test the other deployments. It relies on Kubernetes DNS to find the other deployments in the cluster and test for basic functionality.
