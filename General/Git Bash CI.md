# Git Bash quick push and tag

A useful quick script to push new code and a tagged container.

## Overview

Online continuous builds and deployment is great but can be slow. Well to slow if you want to see and test your new code as quick as you can.
With kubernetes yaml files you ideally want to add a unique tagged container name to deploy just so that you know the exact state of you application.

My script example can be improved and it is by no way perfect but it works for me. It commits the code tag it, then it builds the container, tag the container with the same tag and the push the container. Ready to be deployed.

### The git bash script

```bash
#!/bin/bash
# open git bash
#$ cd /c/vsrepo/usenetcorelog
#run: sh gitBash.sh
git branch
branchtag=$(git log -1 --pretty=%h)
echo Current source tag $branchtag
FILES=*
for file in $FILES
do echo $(basename $file)
done
#prompt user for commit message
read -r -p 'Commit message: ' desc
#track all files
#git add .
#track deletes
git add -u
git commit -m "$desc"
#push to origin
git push origin master
echo ++++++
branchtag=$(git log -1 --pretty=%h)
echo New source tag $branchtag
git tag "$branchtag"
git push origin "$branchtag"
echo +++++++++++++
#####Docker stuff now####
NAME=dockerdelange/blazortestlog
IMG=$NAME:$branchtag
#LATEST=NAME:latest
#read -r -p 'Docker user: ' DockerUser  # prompt user for docker user
#read -r -p 'Docker password: ' DockerPassword  # prompt user for docker user
echo 1 Build image
echo -----------
docker build -t $IMG .
echo 2 Tag image
echo -----------
docker tag $IMG
echo 3 Push image
echo -----------
docker push $IMG
#echo 4 login to dockerhub image
#echo -----------
#docker log -u $DockerUser -p DockerPassword
echo ------done------
read
```
