# Python helper Library to log data to LogyHog via api (LogminiApi)

## Introduction

It will be almost impossible to interact in a effective manner with the API without one of the helper libraries available. The helper libraries do a lot of behind the scenes work when a call is made to ensure the data is valid and ready to go.

### Interacting with LogyHog logging API

The python library is called **httpPythonLog3**.
When **httpPythonLog3** interacts with **LogMiniAPI** the data sent to **LogMiniAPI** gets stored in the **Logyhog** MySQL database.
To View the stored data that you logged, go to the logstuff MVC application and select your the project->Task->dataset.
The **datasets** will then be plotted for you by the MVC application.

***A driver for the API can be in any language if it can make http - https calls and receive/send json data.***

### HttpPythonLog getting started

1. Clone/download the **source code**.

     ```sh
     git clone https://cfdelange@bitbucket.org/cfdelange/httppythonlog3.git
     ```

2. pip install it to import it to your Python source code.

### Setup the Python driver in your Python environment

Navigate to the folder containing the setup.py file then run...

```sh
C:\Workspace\HttpPythonLog>pip install -e .
```

***Note:*** The full stop at the end o f the above commnad *is* part of the command.

To uninstall (from anywhere in your Python 3.6 environment run):

```sh
C:\>pip uninstall HttpPythonLog
Uninstalling HttpPythonLog-0.1:
Would remove:
c:\python27\lib\site-packages\httppythonlog.egg-link
Proceed (y/n)? y
Successfully uninstalled HttpPythonLog-0.1
```

***

### How to create and log to LogyHog datasets

Once you have the LogyHog project up and running and you are able to navigate to it in your web browser. You can start getting ready to log data.

1. ***New Project:***: Go to **Logyhog** in your web browser. One the Home page there is a create a **Project** textbox and button. For example "MyTestProject".

![image info](/images/newproject.jpg)

2. ***New Task:*** Each **Project** can hold multiple Tasks. Create a **Task** in the **Project** web page by clicking the - View Edit Tasks button. This wil take you to the **Task** page. After the task is created you will see the **TokenKey** and **NameKey**, these keys will be used to get access to the logging API later.

![image info](/images/newtask.jpg)

Note: LogyHog is not ready to be logged to. Move to your Python install now and your logging environment.

3. A **Task** holds **Datasets**. **Datasets** gets created programmatically when interfacing with the API. A **Dataset** can hold multiple **types of telemetry** for example.
    * Rectangles
    * Text
    * XY
    * Y vs datetime data

4. Python sample code below show how to use the installed python library **httppythonlog3**

     ```python
     # Import the library into your code File
    from httppythonlog.HttPython import IP_token_Task_User

    if __name__ == "__main__":
        # TokenKey and NameKey added, and the location where your logyhog project is running.
        logyhogLogger = IP_token_Task_User("http://<your ip>", "415-AVCNWS", "raspberry5")
        # mystery data that I collected
        theDataY = [1, 3, 4, 4, 5, 34, 45, 45]
        # Now I want to log this data, this could be some temperature data for example
        for yValue in theDataY:
            logyhogLogger.y("myDataset", yValue)
        # Done
     ```

5. From the example code above, in the second last line, the **Dataset** name is plain text, "myDataset". The python driver will create that **Dataset** name for me a reuse it to log the data into. A dataset will be created in the project, see the project web page:

![image info](/images/datasetCreated.jpg)

6. The data plotted will look like this if you click on the Y data button where the dataset is displayed.

![image info](/images/plottedydata.jpg)

7. To log XY data you use the below command abd the dataset will be created if needed.

     ```python
     logstuff_driver.xy("dataset_name1000", 40, 5)
     ```

8. The above code will create the *dataset_name1000* dataset and add xy coordinates (40, 5) to it.

### Dataset automatic error checking and feedback

#### How to set up XY limits/constraints for a specific dataset

***setlimits("dataset_name", x_high_limit, y_high_limit, x_low_limit, y_low_limit)***

1. The below command will set a rectangle limits around your xy data.

    ```python
    v = logstuff_driver.setlimits_dataset("dataset_name1000", 50, 0, 10, 10)
    ```

2. log some more xy values to it and check on logstuff if the pass or fail

     ```python
     logstuff_driver.xy("dataset_name1000", 40, 5)
     ```

3. To log text to the same dataset.

    ```python
    logstuff_driver.text("dataset_name1000", "Text in my dataset")
    ```

4. To log text to a new, different dataset, the dataset will be created if it does not exist.

    ```python
    logstuff_driver.text("dataset_name1001", "Text in my dataset")
    ```

5. To log y vs datetime values.

    ```python
    logstuff_driver.y("dataset_name1001", 7)
    ```

***
