# Libraries to assist in logging data to Logyhog api (LogminiApi)

## Introduction

There are currently two libraries to assist in logging data to Logminiapi. A Python 3 library and and C# netstandard 2.1 library.

## C# netstandard 2.1

### About nuget and packages for user API interaction

HttpsNet21Log is a nuget package that assists in logging to LogyHog user api (logminiapi). HttpsNet21Log is a netstandard 2.1 package.

Download and install instructions are on the nuget package page where the package information is displayed.

### HttpsNet21Log use case examples

As shown in the the diagram below. You add the nuget package to your c# project use it in your code and then your can start logging to the user API.

![image info](/images/nugetcodekubernetes.jpg)


### Some general engineering notes regarding nuget

* To build a nuget package from a project in Visual Studio go to the Project -> Properties -> Package user interface or set it up in your project file the project file ends with .csproj and the setup looks something like this.

     ```powershell
     <Project>
         <PropertyGroup>
             <TargetFramework>netstandard2.1</TargetFramework>
             <GenerateAssemblyInfo>false</GenerateAssemblyInfo>
             <Authors>Yourname</Authors>
             <Description>Your package description</ Description>
             <GeneratePackageOnBuild>true</GeneratePackageOnBuild>
             <Version>1.0.2.2.2</Version>
             <PackageRequireLicenseAcceptance>false</PackageRequireLicenseAcceptance>
         </PropertyGroup>

     </Project>
     ```

### To push your package

> You need to create a nuget account then get a nuget api key.

* To push the package from you computer open a command line.
* Navigate to the folder containing the nuget package.
* Run the below example command with your package credentials.

    ```powershell
    dotnet nuget push YourPackageName.nupkg -k YourApiKeyGoesHere -s https://api.nuget.org/v3/index.json
    ```

* dotnet cli will output a confirmation message if all went well and the package was pushed
* There are much more to nuget packages...

***
